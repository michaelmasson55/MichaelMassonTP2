﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculMaths
{
    class Factorielle
    {
        public static int metFactorielle(int nbr)
        {
            return FactorielleRecurs(nbr, nbr);
        }

        private static int FactorielleRecurs(int nbr, int currentNbr)
        {
            if (nbr <= 2){
                return currentNbr;
            }else{
                return FactorielleRecurs(nbr - 1, currentNbr * (nbr - 1));
            }
        }
    }
}
