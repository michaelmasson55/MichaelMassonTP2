﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculMaths
{
    class Program
    {
        static void Main(string[] args)
        {
            DeuxNombres mesDeuxNombres = new DeuxNombres(45, 75);
            Console.WriteLine("La somme est : " + mesDeuxNombres.Somme());
            Console.WriteLine("Le produit est : " + mesDeuxNombres.Produit());
            Console.WriteLine("Le PGCD est : " + mesDeuxNombres.PGCD());
            Console.WriteLine("Le PPCM est : " + mesDeuxNombres.PPCM());
            Console.WriteLine("Le MAX est : " + mesDeuxNombres.MAX(1, 2));
            Console.WriteLine("Le MIN est : " + mesDeuxNombres.MIN(1, 2));
            Console.WriteLine("La Factorielle est : " + Factorielle.metFactorielle(4));
            Console.WriteLine("Appuyez sur une touche pour continuer ...");
            Console.ReadKey();
        }
    }
}
